var gulp = require('gulp'),
	jade = require('gulp-jade'),
  watch = require('gulp-watch'),
  stylus = require('gulp-stylus'),
  concat = require('gulp-concat'),
  plumber = require('gulp-plumber'),
  uglyfly = require('gulp-uglyfly'),
  imagemin = require('gulp-imagemin'),
  concatCss = require('gulp-concat-css'),
  minifyCSS = require('gulp-minify-css'),
  pngquant = require('imagemin-pngquant'),
  spritesmith = require('gulp.spritesmith'),
  server = require('gulp-server-livereload'),
  autoprefixer = require('gulp-autoprefixer');

// jade
gulp.task('jade', function () {
	return gulp.src('app/pages/*.jade')
    .pipe(plumber())
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest('dist'))
});

// Stylus
gulp.task('stylus', function () {
  gulp.src('app/styles/*.styl')
    .pipe(stylus({
      compress: false
    }))
     .pipe(autoprefixer({
      browsers: ['last 15 versions'],
      cascade: false
    }))
    .pipe(plumber())
    .pipe(gulp.dest('app/styles/css/'));
});

// sprite
gulp.task('sprite', function() {
    var spriteData = 
    gulp.src('app/images/*.png')
      .pipe(spritesmith({
          imgName: 'sprite.png',
          cssName: 'sprite.styl',
          cssFormat: 'stylus',
          algorithm: 'binary-tree',
          padding: 0,
          cssVarMap: function(sprite) {
            sprite.name = 's-' + sprite.name
          }
      }));

    spriteData.img
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant({
         quality: '65-100', speed: 4
      })]
    }))
    .pipe(gulp.dest('dist/img/sprite/'));
    spriteData.css.pipe(gulp.dest('app/sprite/'));
});

// Concat JS
gulp.task('concatjs', function() {
  return gulp.src('app/scripts/*.js')
    .pipe(concat('common.min.js'))
    .pipe(uglyfly())
    .pipe(gulp.dest('dist/scripts'));
});

// concat css
gulp.task('concatCss', function () {
  gulp.src('app/styles/css/*.css')
    .pipe(concatCss("bundle.css"))
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist/styles/'));
});

// server
gulp.task('server', function() {
  gulp.src('dist')
    .pipe(server({
      livereload: true,
      open: true,
      port: 3000
    }))
});

// Watch
gulp.task('watch', function () {
    gulp.watch('app/pages/*.jade', ['jade']);
    gulp.watch('app/styles/*.styl', ['stylus']);
    gulp.watch('app/styles/css/*.css', ['concatCss']);
    gulp.watch('app/scripts/*.js', ['concatjs']);
    gulp.watch('app/images/*.png', ['sprite']);
});

gulp.task('default',['jade', 'server', 'watch', 'sprite']);